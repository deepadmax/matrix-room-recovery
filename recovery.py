import json
import tkinter as tk

from rich import print
from rich.markdown import Markdown

from tkinter import filedialog
from getpass import getpass
from nio.crypto.key_export import decrypt_and_read


print('[b blue]Where are your exported keys?[/b blue]')
print('[i]Leave empty to open file dialog.[/i]')
keys_path = input('>> ')
print()

if not keys_path:
    root = tk.Tk()
    root.withdraw()  # Hide the root window
    keys_path = filedialog.askopenfilename()

print('[b blue]What passphrase did you use?[/b blue]')
keys = json.loads(decrypt_and_read(keys_path, getpass('>> ')))
print(f'[green]{len(keys)} keys loaded.[/green]')
print()

print('[b u]List of rooms[/b u]')
room_ids = set([key['room_id'] for key in keys])
domains = set()

for room_id in room_ids:
    _, _, domain = room_id.partition(':')
    domains.add(domain)

vias = '&'.join(f'via={domain}' for domain in domains)
for room_id in room_ids:
    room_link = f'https://matrix.to/#/{room_id}?{vias}'
    md = Markdown(f'[{room_id}]({room_link})')
    print(md)

print()
print(f'[green]{len(room_ids)} rooms found.[/green]')
