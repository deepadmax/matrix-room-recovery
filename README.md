# Matrix Room Recovery
Has your homeserver faced total destruction and you've lost all your rooms?
No worries! If your rooms are present on other homeservers, they can be recovered.
Unfortunately, you do need to interact with each room, which does mean you need
to know their IDs. This little script helps you extract room IDs from exported room keys.

1. Export your room keys from either of your clients.
2. Run `python recovery.py`.
3. Enter the path to your exported keys.
4. Links to all rooms will be printed to the terminal.
